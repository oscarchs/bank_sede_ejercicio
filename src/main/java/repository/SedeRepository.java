package repository;

import java.util.Collection;

import domain.Sede;

public interface SedeRepository extends BaseRepository<Sede, Long> {
	
	Sede findByAdress(String adress);
	Collection<Sede> findBySedeId(Long sedeId);
}
