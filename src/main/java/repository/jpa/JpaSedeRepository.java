package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.SedeRepository;
import domain.Sede;

@Repository
public class JpaSedeRepository extends JpaBaseRepository<Sede, Long> implements
		SedeRepository {

	@Override
	public Sede findByAdress(String adress) {
		String jpaQuery = "SELECT a FROM Sede a WHERE a.adress = :adress";
		TypedQuery<Sede> query = entityManager.createQuery(jpaQuery, Sede.class);
		query.setParameter("adress", adress);
		return getFirstResult(query);
	}

	@Override
	public Sede findBySedeId(Long sedeId) {
		String jpaQuery = "SELECT a FROM Sede a JOIN a.banks p WHERE p.id = :sedeId";
		TypedQuery<Sede> query = entityManager.createQuery(jpaQuery, Sede.class);
		query.setParameter("sedeId", sedeId);
		return query.getResultList();
	}
}