package form;

import java.util.Collection;

import domain.Account;
import domain.Sede;

public class CreateAccountForm {
	private Collection<Long> ownerIds;

	private Account account;

	private Sede sede;

	public Collection<Long> getOwnerIds() {
		return ownerIds;
	}

	public void setOwnerIds(Collection<Long> ownerIds) {
		this.ownerIds = ownerIds;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Sede getSede() {
		return this.sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}
}
