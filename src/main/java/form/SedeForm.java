package form;

import java.util.Collection;

import domain.Account;
import domain.Sede;
import domain.Bank;

public class CreateSedeForm {
	private Bank bank;

	private Sede sede;

	public Bank getBank() {
		return this.bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public Sede getSede() {
		return this.sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}
}