package form;

public class TransferForm {
	private String sourceAccountNumber;

	private String targetAccountNumber;

	private String sedeAdress;

	private Double amount;

	public String getSourceAccountNumber() {
		return sourceAccountNumber;
	}

	public void setSourceAccountNumber(String sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}

	public String getTargetAccountNumber() {
		return targetAccountNumber;
	}

	public void setTargetAccountNumber(String targetAccountNumber) {
		this.targetAccountNumber = targetAccountNumber;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getSede() {
		return this.sedeAdress;
	}

	public void setSede(String sedeAdress) {
		this.sedeAdress = sedeAdress;
	}
}
