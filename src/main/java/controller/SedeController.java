package controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import service.TransferService;
import domain.Sede;
import form.CreateAccountForm;
import form.SedeForm;

@Controller
public class SedeController {

	@Autowired
	TransferService transferService;

	@Autowired
	AccountService accountService;

	@RequestMapping(value = "/register-sede", method = RequestMethod.POST)
	String createSede(@ModelAttribute SedeForm createSede, ModelMap model) {
		sedeService.createSede(createSede.get, createSede.getSede());
		return "add-sede";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	String home(ModelMap model) {
		return "home";
	}
}
